import sys
import re

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    #create the configuration variables
    spark_conf = SparkConf().setAppName("Films Locations").setMaster("local[8]")
    spark_context = SparkContext(conf=spark_conf)

    #cretae the logger variable
    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    '''
    Split the csv with the ; delimiter
    map to create an array only with the movie title's
    create a tuple and sum the repetitions
    '''
    titles = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: re.split(";(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", line)) \
        .map(lambda array: array[0]) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b)

    '''
    filter the previous titles by the number of appearance
    and sort it decreasing
    '''
    titles_filter = titles \
        .filter(lambda number: number[1] >= 20) \
        .sortBy(lambda pair: pair[1], ascending=False) \

    '''
    calculate the number of localizations in the entire csv
    '''
    number_loc = titles_filter \
        .map(lambda tuple: tuple[1]) \
        .reduce(lambda a, b: a + b)

    #calculate the number of unique films
    number_films = len(titles.collect())

    #save as textfile
    titles_filter.saveAsTextFile("/home/master/mdscalabledataprocessing/pySpark/outFilms")

    for (word, count) in titles_filter.take(10):
        print("%s: %i" % (word, count))

    print("Total number of films: " + str(number_films))
    print("The average of film locations per film: " + str(number_loc/number_films))

    spark_context.stop()