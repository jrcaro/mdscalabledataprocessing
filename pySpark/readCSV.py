from pyspark.sql import SparkSession
from pyspark.sql import functions

def main() -> None:

    spark_session = SparkSession\
        .builder\
        .master("local[2]")\
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    data_frame = spark_session\
        .read\
        .format("csv")\
        .options(header='true', inferschema='true')\
        .load("/home/juan/Descargas/ejemplo.csv")\
        .persist()

    data_frame.printSchema()
    data_frame.show()

    print("data types: " + str(data_frame.dtypes))

    data_frame\
        .describe()\
        .show()

    data_frame.select("Name").show()

    data_frame.select("Name",data_frame["Age"] + 1).show()

    data_frame.select(functions.length(data_frame["Name"]) > 4).show()

if __name__ == '__main__':
    main()