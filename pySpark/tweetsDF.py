import sys
from pyspark.sql import SparkSession, functions


def main() -> None:
    spark_session = SparkSession \
        .builder \
        .master("local[8]") \
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    data_frame = spark_session \
        .read \
        .options(header='false', inferschema='true') \
        .option("delimiter", "\t") \
        .csv(sys.argv[1]) \
        .persist()

    common_words = {"the", "and", "to", "of", "a", "in", "is", "that", "for",
                    "I", "you", "it", "with", "on", "as", "are", "be", "this",
                    "was", "have", "or", "at", "not", "your", "from", "we", "by",
                    "will", "can", "but", "they", "an", "he", "all", "has", "if",
                    "their", "one", "do", "more", "my", "his", "so", "there",
                    "about", "which", "when", "what", "out", "up", "our", "who",
                    "also", "had", "time", "some", "would", "were", "like", "been",
                    "just", "her", "new", "other", "them", "she", "people", "these",
                    "no", "get", "how", "me", "into", "than", "only", "its",
                    "most", "may", "any", "many", "make", "then", "well", "first",
                    "very", "over", "now", "could", "after", "even", "because", "us",
                    "said", "good", "way", "two", "should", "work", "use", "RT", "-", "&"}

    #change the default name of the columns
    df = data_frame\
        .withColumnRenamed("_c0", "id") \
        .withColumnRenamed("_c1", "user") \
        .withColumnRenamed("_c2", "tweet") \
        .withColumnRenamed("_c3", "date") \
        .withColumnRenamed("_c4", "long") \
        .withColumnRenamed("_c5", "lat") \
        .withColumnRenamed("_c6", "city")

    #print the type of the data in the columns and the dataframe
    df.printSchema()
    df.show()

    print("DataFrame:")
    print("Most common words:")
    '''
    Select the tweet column and overwrite it with the split by whitespaces
    (words). Filter by common words, delete the URLS and the mark symbols.
    Finally group by the column tweet and count the number of repetitions
    '''
    df.withColumn('tweet', functions.explode(functions.split(functions.col("tweet"), ' '))) \
        .filter(~functions.col('tweet').isin(common_words)) \
        .filter(~functions.col('tweet').rlike("http*")) \
        .withColumn('tweet', functions.explode(functions.split(functions.col("tweet"), ':'))) \
        .groupBy('tweet') \
        .count() \
        .sort('count', ascending=False) \
        .show(10)

    '''
    Select the user column and group by the unique names and count it.
    '''
    print("User who has written more tweets:")
    df.select('user') \
        .groupBy('user') \
        .count() \
        .sort('count', ascending=False) \
        .show(1)

    '''
    Create a new column named length where store the length of the tweets,
    sort ascending and select the first one (minimum)
    '''
    print("Shortest tweet and the user who has written it:")
    df.withColumn('length', functions.length(functions.col("tweet"))) \
        .sort('length', ascending=True) \
        .select(["tweet", "user", "date", "length"]) \
        .show(1, truncate = False)


if __name__ == '__main__':
    main()