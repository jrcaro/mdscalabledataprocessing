import sys

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    spark_conf = SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    #load the first csv in a RDD array with only two columns, name and code of the countries
    countries = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split(',')) \
        .map(lambda airport: (airport[1], airport[2]))

    print("RDD")
    print("Number of large airports by countries:")

    '''
    Split the airport csv and filter by the type. Create a tuple with the nineth column
    and a counter. Sum the repeated code of country and joined with the previous dataset.
    Finaly select only the columns name and code.
    '''
    airports = spark_context \
        .textFile(sys.argv[2]) \
        .map(lambda line: line.split(',')) \
        .filter(lambda type: '"large_airport"' in type[2]) \
        .map(lambda code: (code[8], 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .join(countries)\
        .map(lambda x: (x[1][1], x[1][0])) \
        .sortBy(lambda pair: pair[1], ascending=False) \
        .take(10)

    for (word, count) in airports:
        print("%s: %i" % (word, count))

    spark_context.stop()