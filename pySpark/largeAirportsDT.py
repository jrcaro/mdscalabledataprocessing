from pyspark.sql import functions, SparkSession
import sys


def main() -> None:
    spark_session = SparkSession \
        .builder \
        .master("local[8]") \
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    #load the countries csv to a dataframe with the header flag true
    countries_df = spark_session \
        .read \
        .options(header='true', inferschema='true') \
        .option("delimiter", ",") \
        .csv(sys.argv[1]) \
        .persist()

    # load the airport csv to a dataframe with the header flag true
    airport_df = spark_session \
        .read \
        .options(header='true', inferschema='true') \
        .option("delimiter", ",") \
        .csv(sys.argv[2]) \
        .persist()

    countries_df.show() #print the dataframes
    airport_df.show()

    '''
    Select the country code from the airport dataframe and filter by type of airport.
    Group by the previous code and count it. Finally join the other dataframe and select only
    the columns name (of the country) and count (frequency)
    '''
    print("Datraframe")
    print("Number of large airports by countries:")
    airport_df.select('iso_country') \
        .filter(functions.col("type") == 'large_airport') \
        .groupBy('iso_country') \
        .count() \
        .sort('count', ascending=False) \
        .join(countries_df, functions.col("iso_country") == functions.col("code"), 'inner') \
        .select(['name', 'count']) \
        .show(10)


if __name__ == '__main__':
    main()