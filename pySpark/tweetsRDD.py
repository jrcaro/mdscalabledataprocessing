import sys
import re

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    spark_conf = SparkConf().setAppName("RDD tweets").setMaster("local[8]")
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    common_words = {"the", "and", "to", "of", "a", "in", "is", "that", "for",
                    "i", "you", "it", "with", "on", "as", "are", "be", "this",
                    "was", "have", "or", "at", "not", "your", "from", "we", "by",
                    "will", "can", "but", "they", "an", "he", "all", "has", "if",
                    "their", "one", "do", "more", "my", "his", "so", "there",
                    "about", "which", "when", "what", "out", "up", "our", "who",
                    "also", "had", "time", "some", "would", "were", "like", "been",
                    "just", "her", "new", "other", "them", "she", "people", "these",
                    "no", "get", "how", "me", "into", "than", "only", "its",
                    "most", "may", "any", "many", "make", "then", "well", "first",
                    "very", "over", "now", "could", "after", "even", "because", "us",
                    "said", "good", "way", "two", "should", "work", "use","rt", "-", "&", ""}

    file = spark_context.textFile(sys.argv[1])

    '''
    split first by the tab expresion and create the array with only
    the tweet. Then split by whitespaces to achive the words individually.
    Delete the URLs, some symbols and the common words. Now create a
    tuple and add the reapeted ones. Finally sort descending
    '''
    words = file \
        .map(lambda line: re.split("[\t]+", line)) \
        .map(lambda array: array[2]) \
        .flatMap(lambda line: re.split("[\s]+", line)) \
        .filter(lambda url: not re.compile("^[http]").match(url)) \
        .flatMap(lambda line: re.split("[,:?!]+", line)) \
        .filter(lambda l: l.lower() not in common_words) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .sortBy(lambda pair: pair[1], ascending=False) \
        .take(10)

    print("RDD")
    print("Most commons words: ")
    for (word, count) in words: #print the common words in the tweets and their frec
        print("%s: %i" % (word, count))

    '''
    split by tab, create an array (map) with only the users, create a tuple
    and count the frequency of appearance. Finally sort descending and
    take the first
    '''
    users = file \
        .map(lambda line: re.split("[\t]+", line)) \
        .map(lambda array: array[1]) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .sortBy(lambda pair: pair[1], ascending=False) \
        .take(1)

    print()

    for (word, count) in users:
        print("User who has written more tweets: %s - %i" % (word, count))

    '''
    split by tab, create a tuple with all the information of the tweets and
    the length of the tweet and keep the min
    '''
    tweet = file \
        .map(lambda line: re.split("[\t]+", line)) \
        .map(lambda tweet: (tweet, len(tweet[2]))) \
        .min(lambda x: x[1])

    print() #print the data
    print("Shortest tweet by " + tweet[0][1] + " with lenght of " + str(tweet[1]) + " on " + tweet[0][3])

    spark_context.stop()