import sys

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    # create the configuration variables
    spark_conf = SparkConf().setAppName("Spanish airports").setMaster("local[8]")
    spark_context = SparkContext(conf=spark_conf)

    # cretae the logger variable
    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    '''
    first split the CSV with the map.
    filter by the country column
    map by the type column to create an array only with them
    map to create a tuple
    sum the tuple and save to file
    '''
    spark_context.textFile(sys.argv[1]) \
        .map(lambda line: line.split(',')) \
        .filter(lambda type: '"ES"' in type[8]) \
        .map(lambda airport: airport[2]) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .saveAsTextFile("outAirport")

    spark_context.stop()


