package org.masterbigdata.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.text.DecimalFormat;
import java.util.List;

public class FilmLocationSF {
    static Logger log = Logger.getLogger(SpanishAirports.class.getName());
    static final int INDEX_TITLE = 0;
    static final String PATH_OUT =
            "/home/master/mdscalabledataprocessing/sparkMavenJava/src/main/java/org/masterbigdata/spark/outputFilms";

    public static void main(String[] args) {
        Logger.getLogger("org").setLevel(Level.OFF);

        //exception if there arent a argument
        if(args.length < 1){
            log.fatal("Syntax Error: there must be one argument (a file name or a directory)");
            throw new RuntimeException();
        }

        //Spark configuration object
        SparkConf sparkConf = new SparkConf().setAppName("Film Locations").setMaster("local[8]");

        //Spark context
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        //Read the csv file
        JavaRDD<String> lines = sparkContext.textFile(args[0]);

        JavaPairRDD<String, Integer> titles = lines
                .map(line -> line.split(";(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1))//split the csv with ; delimiter
                .map(array -> array[INDEX_TITLE]) //map the array with the titles of the movies
                .mapToPair(word -> new Tuple2<>(word, 1)) //create the tuple
                .reduceByKey(Integer::sum); //sum the tuples

        JavaPairRDD<Integer, String> titles_filter = titles
                .filter(number -> number._2 >= 20) //filter the number of movies
                .mapToPair(pair -> new Tuple2<>(pair._2(), pair._1())) //swap the positions of the tuples to sort it
                .sortByKey(false);

        int number_loc = titles_filter
                .map(tuple -> tuple._1())
                .reduce((a, b) -> a + b); //calculate the total number of locations in the csv

        List<Tuple2<Integer, String>> output = titles_filter.take(10); //take the ten first positions
        long number_films = titles.collect().size(); //calculate the number of films
        float avg_loc = (float) number_loc/number_films; //calcuklate the average of locations per film

        titles_filter.saveAsTextFile(PATH_OUT); //save as textfile

        for (Tuple2<?, ?> tuple : output) {
            System.out.println("(" + tuple._1() + ", " + tuple._2() + ")");
        }


        System.out.println("Total number of films: " + number_films);
        System.out.println("TThe average of film locations per film: " + avg_loc);
        sparkContext.stop();
    }
}

