package org.masterbigdata.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

public class SpanishAirports {
    static Logger log = Logger.getLogger(SpanishAirports.class.getName());
    static final int INDEX_COUNTRY = 8;
    static final int INDEX_TYPE = 2;
    static final String PATH_OUT =
            "/home/master/mdscalabledataprocessing/sparkMavenJava/src/main/java/org/masterbigdata/spark/outAirports";

    public static void main(String[] args) {
        Logger.getLogger("org").setLevel(Level.OFF);

        //exception if there arent a argument
        if(args.length < 1){
            log.fatal("Syntax Error: there must be one argument (a file name or a directory)");
            throw new RuntimeException();
        }

        //Spark configuration object
        SparkConf sparkConf = new SparkConf().setAppName("Spark Spanish Airport").setMaster("local[8]");

        //Spark context
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        //Read the csv file
        JavaRDD<String> lines = sparkContext.textFile(args[0]);

        lines.map(line -> line.split(",")) //split the CSV data
                .filter(country -> country[INDEX_COUNTRY].equalsIgnoreCase("\"ES\"")) //filter by column country
                .map(array -> array[INDEX_TYPE]) //map the type of airport
                .mapToPair(word -> new Tuple2<>(word, 1)) //create the tuple with the type of airport
                .reduceByKey(Integer::sum) //add the equals tuples
                .saveAsTextFile(PATH_OUT); //save the output

        sparkContext.stop();
    }
}
